Two programs:
	KinectRecord
		- records RGB data, audio, and skeleton data from Kinect
		- RGB data is converted to AVI, then it is combined with audio WAV file to create AVI file with video and sound
		- skeleton data is written to a GZ file for internal use with KinectPlayback and a BVH file for external use
		- KEY summary of code:
			- launches instance of Kinect
			- listens for events "KinectSensor.SkeletonFrameReady" and "KinectSensor.SkeletonFrameReady" which push the respective RGB data and skeleton data.
			- Before recording session, Kinect collects information about the recording session from the user, this data is saved to a text file
			- After recording session, Kinect collects any additional information abou the recording session from the user. The data is saved to the same text file
			- This data is then recorded:
				- the audio data is polled: on a separate thread, it reads from a stream initialized by KinectSensor.AudioSource.Start
				- three windows: MainWindow, ColorFrameWindow, SkeletonFrameWindow
					- the ColorFrameWindow and SkeletonFrameWindow are initialized on separate background threads
					- ColorFrameWindow streams the RGB data as an Image 
					- SkeletonFrameWindow streams the skeleton data as a SkeletonPainter3D object
					- the windows are run on separate threads to fix flickering problems which resulted from running the 3D skeleton (on the SkeletonFrameWindow)
					and RGB image (on the ColorFrameWindow) on the same thread
				
	KinectPlayback
		- does the playback of data recorded by KinectRecord
		- from AVI player window, select Open to choose a WAV file for playback
			- it will then play back the AVI file and GZ file in-sync with one another
		- KEY summary of code:
			- three windows: MainWindow, AviWindow, SkeletonFrameWindow
				- AviWindow provides the code for the front-end of the media player; it plays back the AVI file
				- SkeletonFrameWindow plays back the skeleton data through the SkeletonPainter3D object
				- MainWindow provides the code that synchronizes the playback of the AVI file and the skeleton data
				- MainWindow also has annotation buttons: Bad Form, Unsafe, Other
					- these buttons add timestamped annotations to the text file generated from the recording session
					- BadForm adds "Bad Form" 
					- Unsafe adds "Unsafe" 
					- Other allows user to add free-form information
		
3rd party libraries used by the programs are stored in the "packages" folder

Requires Xvid Codec to be installed on system: https://www.xvid.com/
	- after installed search in Windows for and then launch "Configure Encoder"
		- go to: Other Options -> Encoder -> deselect "Display encoding status"

Used BVHViewer for viewing the BVH files: http://www.developmentinmotion.nl/software/bvhviewer/
	- however, there are various available BVH viewers
