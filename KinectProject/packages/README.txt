Links to sources: 
	Kinect2BVHSVN: https://kinect2bvh.codeplex.com/releases/view/113388
	SharpAvi-2.0: http://sharpavi.codeplex.com/
	FFmpeg: http://ffmpeg.zeranoe.com/builds/
	
	Used nuget to pull all of the dependent packages: https://www.nuget.org/packages/TCD.Kinect/
		TCD.Kinect.1.2.2
			For reference, see http://kuchenzeit.wordpress.com/2012/10/12/tcd-kinect/
		TCD.Mathematics.1.2.0
		HelixToolkit.2013.1.10.1
