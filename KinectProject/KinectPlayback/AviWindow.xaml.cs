﻿// default namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// additional namespaces
using System.Windows.Threading;
using System.IO;
using Microsoft.Win32;
using System.Threading;


namespace KinectPlayback
{
    // adapted from "Custom Media Player in WPF (Part 1)":
    // http://www.codeproject.com/Articles/731715/Custom-Media-Player-in-WPF-Part
    public partial class AviWindow : Window
    {
        DispatcherTimer timer;

        public delegate void timerTick();
        timerTick tick;

        bool isDragging = false;
        bool fileIsPlaying = false;
        string sec, min, hours;

        KinectPlayback.MainWindow mw;

        public AviWindow(KinectPlayback.MainWindow mw)
        {
            InitializeComponent();

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(timer_Tick);
            tick = new timerTick(changeStatus);

            this.mw = mw;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Dispatcher.Invoke(tick);
        }


        // visualizes progressBar 
        void changeStatus()
        {
            if (fileIsPlaying)
            {
                updatecurrentTimeTextBlock();
            }
        }


        // opens the file
        private void openFileButton_Click(object sender, RoutedEventArgs e)
        {
            Stream checkStream = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Supported File Types(*.avi)|*.avi;";
            dlg.InitialDirectory = 
                //@"C:\Users\misc\Documents\wiinwalk-tracking\KinectProject6\KinectRecord\bin";
                @"C:\Users\" 
                + Environment.UserName 
                + @"\Documents\wiinwalk-tracking\KinectProject\KinectRecord\bin"
// C# Debug and release mode: http://stackoverflow.com/a/19053995
#if DEBUG
                + @"\Debug";
#else
                + @"\Release";
#endif
            
            // Show open file dialog box 
            if ((bool)dlg.ShowDialog())
            {
                try
                {
                    if ((checkStream = dlg.OpenFile()) != null)
                    {
                        mediaElement.Close();

                        string filePathAvi = dlg.FileName;

                        //--
                        string filePathWithoutExtension = 
                            Shared.Utilities.GetFullPathWithoutExtension(filePathAvi);
                        string filePathWav = filePathWithoutExtension + ".wav";
                        bool isRecordingNotProperlyClosed = 
                            File.Exists(filePathAvi) && File.Exists(filePathWav);

                        if (isRecordingNotProperlyClosed)
                        {
                            mw.WriteLineToMessageLog("Recording was not properly closed");

                            // close file
                            checkStream.Close();

                            string directoryPath = System.IO.Path.GetDirectoryName(filePathAvi);
                            string tempFilePath = directoryPath + "\\" + "new.avi";

                            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo
                            {
                                FileName = "DivFix++.exe",

                                Arguments = " -i " + filePathAvi + " -o " + tempFilePath,

                                CreateNoWindow = true,
                                UseShellExecute = false,
                            };

                            // execute merging of AVI file and WAV file
                            System.Diagnostics.Process process = System.Diagnostics.Process.Start(processStartInfo);
                            process.WaitForExit();

                            File.Delete(filePathAvi);
                            File.Move(tempFilePath, filePathAvi);

                            var ffmpeg = new KinectRecord.FFmpeg();
                            ffmpeg.MergeWavWithAvi(filePathAvi, filePathWav);

                            KinectRecord.SkeletonRecorder.CompressFile(filePathWithoutExtension);

                            
                            int totalNumberOfFrames = 0;

                            using (StreamReader sr = new StreamReader(filePathWithoutExtension + ".bvh"))
                            {
                                string line;
                                while ((line = sr.ReadLine()) != null)
                                {
                                    if (line.Contains("Frame Time:"))
                                        break;
                                }

                                while ((line = sr.ReadLine()) != null)
                                {
                                    totalNumberOfFrames++; 
                                }
                            }

                            string text = File.ReadAllText(filePathWithoutExtension + ".bvh");
                            // https://msdn.microsoft.com/en-us/library/xwewhkd1%28v=vs.110%29.aspx
                            string result = text.Replace("Frames: PLATZHALTERFRAMES", "Frames: " + totalNumberOfFrames);
                            File.WriteAllText(filePathWithoutExtension + ".bvh", result);


                            mw.WriteLineToMessageLog("Recording files fixed");
                        }

                        // assign newly chosen file to mediaElement
                        mediaElement.Source = new Uri(filePathAvi);

                        mediaElement.Play();                        

                        playButton_.Content = "Pause";
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    mw.WriteLineToMessageLog("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        // occurs when the file is opened
        public void mediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            timer.Start();
            fileIsPlaying = true;
            openMedia();
        }

        // opens media,adds file to playlist and gets file info
        public void openMedia()
        {
            InitializePropertyValues();
            try
            {
                string totalSec, totalMin, totalHour;

                #region customizeTime
                if (mediaElement.NaturalDuration.TimeSpan.Seconds < 10)
                    totalSec = "0" + mediaElement.NaturalDuration.TimeSpan.Seconds.ToString();
                else
                    totalSec = mediaElement.NaturalDuration.TimeSpan.Seconds.ToString();

                if (mediaElement.NaturalDuration.TimeSpan.Minutes < 10)
                    totalMin = "0" + mediaElement.NaturalDuration.TimeSpan.Minutes.ToString();
                else
                    totalMin = mediaElement.NaturalDuration.TimeSpan.Minutes.ToString();

                if (mediaElement.NaturalDuration.TimeSpan.Hours < 10)
                    totalHour = "0" + mediaElement.NaturalDuration.TimeSpan.Hours.ToString();
                else
                    totalHour = mediaElement.NaturalDuration.TimeSpan.Hours.ToString();

                if (mediaElement.NaturalDuration.TimeSpan.Hours == 0)
                {
                    endTimeTextBlock.Text = totalMin + ":" + totalSec;
                }
                else
                {
                    endTimeTextBlock.Text = totalHour + ":" + totalMin + ":" + totalSec;
                }

                #endregion customizeTime
            }
            catch { }
            string path = mediaElement.Source.LocalPath.ToString();
            
            double duration = mediaElement.NaturalDuration.TimeSpan.TotalMilliseconds;
            seekSlider.Maximum = duration;
            
            mediaElement.SpeedRatio = speedRatioSlider.Value;
            mediaElement.ScrubbingEnabled = true;

            speedRatioSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(speedRatioSlider_ValueChanged);
        }

        // occurs when the file is done playing
        private void mediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            stopButton_Click(sender, e);
        }

        // initialize properties of file
        void InitializePropertyValues()
        {
            mediaElement.SpeedRatio = (double)speedRatioSlider.Value;
        }

        // seek to desired position of the file
        private void seekSlider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)seekSlider.Value);

            changePostion(ts);
        }

        //mouse down on slide bar in order to seek
        private void seekSlider_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isDragging = true;
            fileIsPlaying = false;
        }

        private void seekSlider_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isDragging)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)seekSlider.Value);
                changePostion(ts);
                fileIsPlaying = true;
            }
            isDragging = false;
        }


        //--- Position changed events ---//

        // A delegate type for hooking up change notifications.
        public delegate void ChangedEventHandler(object sender, TimeSpan ts);

        // An event that clients can use to be notified whenever the
        // elements of the list changes.
        public event ChangedEventHandler PositionChanged;

        // change position of the file
        void changePostion(TimeSpan ts)
        {
            mediaElement.Position = ts;
            updatecurrentTimeTextBlock();

            Task.Run(() =>
            {
                if (PositionChanged != null)
                    PositionChanged(this, ts);
            });
        }
        //---//

        
        // updates the time indicated while playing the file
        void updatecurrentTimeTextBlock()
        {
            #region customizeTime
            if (mediaElement.Position.Seconds < 10)
                sec = "0" + mediaElement.Position.Seconds.ToString();
            else
                sec = mediaElement.Position.Seconds.ToString();


            if (mediaElement.Position.Minutes < 10)
                min = "0" + mediaElement.Position.Minutes.ToString();
            else
                min = mediaElement.Position.Minutes.ToString();

            if (mediaElement.Position.Hours < 10)
                hours = "0" + mediaElement.Position.Hours.ToString();
            else
                hours = mediaElement.Position.Hours.ToString();

            #endregion customizeTime

            seekSlider.Value = mediaElement.Position.TotalMilliseconds;

            if (mediaElement.Position.Hours == 0)
            {
                currentTimeTextBlock.Text = min + ":" + sec;
            }
            else
            {
                currentTimeTextBlock.Text = hours + ":" + min + ":" + sec;
            }
        }


        // change the speed of the playback
        void speedRatioSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaElement.SpeedRatio = speedRatioSlider.Value;
        }

        public void playButton__Click2(object sender, RoutedEventArgs e)
        {
            if (!fileIsPlaying) 
            {
                fileIsPlaying = true;
                mediaElement.Play();
                playButton_.Content = "Pause";
                timer.Start();
            }
            // file is playing
            else              
            {
                fileIsPlaying = false;
                mediaElement.Pause();
                playButton_.Content = "Play";
                timer.Stop();
            }

            if (PlayPause != null)
                PlayPause(sender, e);
        }

        public delegate void PlayPause_EventHandler(object sender, EventArgs e);
        public event PlayPause_EventHandler PlayPause;


        // stop playback of the file
        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            fileIsPlaying = false;
            timer.Stop();
            mediaElement.Stop();
            seekSlider.Value = 0;
            //progressBar.Value = 0;
            currentTimeTextBlock.Text = "00:00";

            playButton_.Content = "Play";
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (Window win in App.Current.Windows)
                win.Close();
        }
    }
}
