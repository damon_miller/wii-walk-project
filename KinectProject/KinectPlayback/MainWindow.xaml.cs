﻿// default namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// additional namespaces
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using Shared;
using Microsoft.Kinect;
using System.Threading;
using KinectRecord;


namespace KinectPlayback
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        AviWindow aviWindow;
        SkeletonFrameWindow skeletonFrameWindow;

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            // disable Window close button
            this.Closing += WindowsCloseButton.DisableWindowClosing;

            aviWindow = new AviWindow(this);
            aviWindow.Show();
            aviWindow.Closing += WindowsCloseButton.DisableWindowClosing;

            skeletonFrameWindow = new SkeletonFrameWindow();
            skeletonFrameWindow.Show();
            skeletonFrameWindow.Closing += WindowsCloseButton.DisableWindowClosing;

            InitializeWindowSizesAndPositions();
            AddLocationChangedEvents();
            
            aviWindow.mediaElement.Volume = 1;
            aviWindow.mediaElement.MediaOpened += InitializeReplay;
            aviWindow.mediaElement.MediaEnded += PlayPauseButton_Click;
            aviWindow.PlayPause += PlayPauseButton_Click;
            aviWindow.stopButton.Click += StopButton_Click;
            aviWindow.PositionChanged += PositionChanged;
            
            aviWindow.badFormButton.Click += BadFormButton_Click;
            aviWindow.unsafeButton.Click += UnsafeButton_Click;
            aviWindow.otherButton.Click += OtherButton_Click;

            // starts off aviWindow as the selected window (just for cosmetic reasons)
            aviWindow.Activate();
        }

        void ExitButton_Click(object sender, EventArgs e)
        {
            CleanFinal();
            
            skeletonFrameWindow.Closing -= WindowsCloseButton.DisableWindowClosing;
            aviWindow.Closing -= WindowsCloseButton.DisableWindowClosing;
            this.Closing -= WindowsCloseButton.DisableWindowClosing;

            skeletonFrameWindow.Close();
            aviWindow.Close();
            this.Close();
        }

        private void InitializeWindowSizesAndPositions()
        {
            double referenceWidth = aviWindow.Width;
            double referenceHeight = aviWindow.Height;
            
            this.Width = referenceWidth;

            skeletonFrameWindow.Width = referenceWidth;
            skeletonFrameWindow.Height = this.Height + referenceHeight;

            SkeletonFrameWindow_UpdateLocation();
            AviWindow_UpdateLocation();
        }

        void AddLocationChangedEvents()
        {
            this.LocationChanged += AviWindow_UpdateLocation;
            this.LocationChanged += SkeletonFrameWindow_UpdateLocation;

            aviWindow.LocationChanged += AviWindow_LocationChanged;
            skeletonFrameWindow.LocationChanged += SkeletonFrameWindow_LocationChanged;
        }

        void RemoveLocationChangedEvents()
        {
            this.LocationChanged -= AviWindow_UpdateLocation;
            this.LocationChanged -= SkeletonFrameWindow_UpdateLocation;

            aviWindow.LocationChanged -= AviWindow_LocationChanged;
            skeletonFrameWindow.LocationChanged -= SkeletonFrameWindow_LocationChanged;
        }

        // updates mainWindow location when aviWindow's location changes
        void AviWindow_LocationChanged(object sender, EventArgs e)
        {
            if (aviWindow.IsActive)
            {
                double referenceLeft = aviWindow.Left;
                double referenceTop = aviWindow.Top;

                // must disable effect of changing mainWindow location on the aviWindow location 
                this.LocationChanged -= AviWindow_UpdateLocation;

                this.Left = referenceLeft;
                this.Top = referenceTop - this.Height;

                // re-enable effect of changing mainWindow location on the aviWindow location 
                this.LocationChanged += AviWindow_UpdateLocation;
            }
        }

        // updates mainWindow location when skeletonWindow's location changes
        void SkeletonFrameWindow_LocationChanged(object sender, EventArgs e)
        {
            if (skeletonFrameWindow.IsActive)
            {
                double referenceLeft = skeletonFrameWindow.Left;
                double referenceTop = skeletonFrameWindow.Top;

                // must disable effect of changing mainWindow location on the skeletonWindow location 
                this.LocationChanged -= SkeletonFrameWindow_UpdateLocation;

                this.Left = referenceLeft - this.Width;
                this.Top = referenceTop;

                // re-enable effect of changing mainWindow location on the skeletonWindow location 
                this.LocationChanged += SkeletonFrameWindow_UpdateLocation;
            }
        }

        // updates aviWindow location in response to change in mainWindow location //
        public void AviWindow_UpdateLocation(object sender, EventArgs e)
        {
            AviWindow_UpdateLocation();
        }

        void AviWindow_UpdateLocation()
        {
            double leftPosition = this.Left;
            double topPosition = this.Top + this.Height;

            aviWindow.Left = leftPosition;
            aviWindow.Top = topPosition;
        }
        //---//

        // updates skeletonFrameWindow location in response to change in mainWindow location //
        public void SkeletonFrameWindow_UpdateLocation(object sender, EventArgs e)
        {
            SkeletonFrameWindow_UpdateLocation();
        }

        void SkeletonFrameWindow_UpdateLocation()
        {
            double leftPosition = this.Left + this.Width;
            double topPosition = this.Top;

            skeletonFrameWindow.Left = leftPosition;
            skeletonFrameWindow.Top = topPosition;
        }
        //---//


        //--- Annotation actions // 

        void BadFormButton_Click(object sender, EventArgs e)
        {
            if (isMediaLoaded)
            {
                AddToMetadata("Bad form");

                WriteLineToMessageLog("Bad form recorded");
            }
        }

        void UnsafeButton_Click(object sender, EventArgs e)
        {
            if (isMediaLoaded)
            {
                AddToMetadata("Unsafe");

                WriteLineToMessageLog("Unsafe recorded");
            }
        }

        TextBoxWindow textBoxWindow;

        void OtherButton_Click(object sender, EventArgs e)
        {
            if (isMediaLoaded)
            {
                textBoxWindow = new TextBoxWindow();
                    
                if (!isPaused)
                {
                    aviWindow.playButton__Click2(this, new RoutedEventArgs());

                    textBoxWindow.ShowDialog();
                    AddToMetadata("Other: " + textBoxWindow.textBoxInput.Text);

                    aviWindow.playButton__Click2(this, new RoutedEventArgs());
                }
                else
                {
                    textBoxWindow.ShowDialog();
                    AddToMetadata("Other: " + textBoxWindow.textBoxInput.Text);
                }

                WriteLineToMessageLog("Other data recorded");
            }
        }

        void AddToMetadata(string message)
        {
            File.AppendAllText(
                GetFilePathWithoutExtensions() + ".txt",
                aviWindow.mediaElement.Position + ": " + message + Environment.NewLine);
        }        
        //---//


        List<SkeletonReplayData> skeletonFrames;
        //long medianTimeElapsedBetweenFrames_milliseconds = 0;
        double medianTimeElapsedBetweenFrames_milliseconds = 0;

        void InitializeReplay(object sender, EventArgs e)
        {
            // if another file has been loaded, then remove the existing loaded file
            Clean();

            string filePath = GetFilePathWithoutExtensions() + ".gz";
            FileInfo fileInfo = new FileInfo(filePath);

            List<double> elapsedTimes = new List<double>();

            //--- Decompress GZ file for playback of skeleton data ---//
            using (FileStream fileToDecompressStream = fileInfo.OpenRead())
            {
                using (GZipStream decompressionStream = 
                    new GZipStream(fileToDecompressStream, CompressionMode.Decompress))
                {
                    using (MemoryStream decompressedStream = new MemoryStream())
                    {
                        decompressionStream.CopyTo(decompressedStream);
                        decompressedStream.Seek(0, SeekOrigin.Begin);

                        using (BinaryReader binaryReader = new BinaryReader(decompressedStream))
                        {
                            BinaryFormatter formatter = new BinaryFormatter();
                            SkeletonReplayData skeletonReplayData;
                            
                            // used to "normalize" timestamps of skeleton data
                            /*skeletonReplayData = 
                                (SkeletonReplayData)formatter.Deserialize(binaryReader.BaseStream);
                            //long BaseTime = skeletonReplayData.millisecondsElapsedSinceLastFrame;
                            double BaseTime = skeletonReplayData.millisecondsElapsedSinceLastFrame;
                            decompressedStream.Seek(0, SeekOrigin.Begin);*/

                            skeletonFrames = new List<SkeletonReplayData>();

                            while (binaryReader.BaseStream.Position != binaryReader.BaseStream.Length)
                            {
                                skeletonReplayData =
                                    (SkeletonReplayData)formatter.Deserialize(binaryReader.BaseStream);
                                //skeletonReplayData.skeleton.Mirror();
                                /*// used to "normalize" timestamps of skeleton data
                                skeletonReplayData.millisecondsElapsedSinceLastFrame -= BaseTime;*/

                                skeletonFrames.Add(skeletonReplayData);

                                if (skeletonFrames.Count > 1) 
                                {
                                    int i = skeletonFrames.Count - 1;

                                    double elapsedTime = 
                                        skeletonFrames[i].millisecondsElapsedSinceLastFrame 
                                        - skeletonFrames[i-1].millisecondsElapsedSinceLastFrame;

                                    elapsedTimes.Add(elapsedTime);
                                }
                            }
                        }
                    }
                }
            }
            //---//

            elapsedTimes.Sort();
            int medianIndex = (-1 + elapsedTimes.Count) / 2;
            medianTimeElapsedBetweenFrames_milliseconds = elapsedTimes[medianIndex];
            StartReplay();
            isMediaLoaded = true;

            string filename = System.IO.Path.GetFileNameWithoutExtension(aviWindow.mediaElement.Source.LocalPath);
            WriteLineToMessageLog(filename + " loaded");
        }

        void Clean()
        {
            isCancelled = true;

            // if paused, then unpause it
            if (isPaused == true)
            {
                PlayPauseButton_Click(this, new EventArgs());
            }

            if (skeletonPlayback_Task != null)
                skeletonPlayback_Task.Wait();  // wait for it to return before continuing            
            
            if (skeletonFrames != null)
            {
                skeletonFrames.Clear();
                skeletonFrames = null;
            }
        }

        async void CleanFinal()
        {
            isCancelled = true;

            // if paused, then unpause it
            if (isPaused == true)
            {
                PlayPauseButton_Click(this, new EventArgs());
            }

            if (skeletonPlayback_Task != null)
                await skeletonPlayback_Task;
        }

        string GetFilePathWithoutExtensions()
        {
            // get file path of video
            string filePath = aviWindow.mediaElement.Source.LocalPath;

            // remove extension from file path
            filePath = System.IO.Path.ChangeExtension(filePath, null);

            return filePath;
        }


        //CancellationTokenSource cancellationTokenSource;
        Task skeletonPlayback_Task = null;
        Boolean isCancelled = true;

        //--- Initializes playback of skeleton data ---//

        public class SkeletonComparer : IComparer<SkeletonReplayData>
        {
            public int Compare(SkeletonReplayData x, SkeletonReplayData y)
            {
                if (x.millisecondsElapsedSinceLastFrame < y.millisecondsElapsedSinceLastFrame)
                    return -1;
                else if (x.millisecondsElapsedSinceLastFrame > y.millisecondsElapsedSinceLastFrame)
                    return 1;
                else
                    return 0;
            }
        }

        static double millisecondsBetweenAviFrameRates = Math.Pow(10, 3) * (1 / (double)KinectRecord.MainWindow.FRAMES_PER_SECOND);
        TimeSpan millisecondsBetweenAviFrameRates_ts = TimeSpan.FromMilliseconds(millisecondsBetweenAviFrameRates);

        void StartReplay()
        {
            isCancelled = false;

            TimeSpan avi_PlaybackPosition = TimeSpan.Zero;

            // gets index of skeletonFrames that corresponds to avi_PlaybackPosition
            Func<int> Get_SkeletonFrameIndex = () =>
            {
                aviWindow.Dispatcher.Invoke(() =>
                {
                    avi_PlaybackPosition = aviWindow.mediaElement.Position;
                });

                SkeletonComparer sc = new SkeletonComparer();
                int index = skeletonFrames.BinarySearch(new SkeletonReplayData(avi_PlaybackPosition.TotalMilliseconds, null), sc);
                if (index < 0)
                    index = ~index;
                if (index == skeletonFrames.Count)
                    --index;

                double upperSpread = skeletonFrames[index].millisecondsElapsedSinceLastFrame - avi_PlaybackPosition.TotalMilliseconds;
                int lowerIndex = Math.Max(0, -1 + index);
                double lowerSpread = avi_PlaybackPosition.TotalMilliseconds - skeletonFrames[lowerIndex].millisecondsElapsedSinceLastFrame;


                if (lowerSpread < upperSpread)
                    if (lowerSpread <= millisecondsBetweenAviFrameRates) {
                        /*this.Dispatcher.InvokeAsync(() => {
                            WriteLineToMessageLog("lower");
                        });*/
                        return lowerIndex;
                    }
                    else
                        return -1;
                else
                    if (upperSpread <= millisecondsBetweenAviFrameRates) {
                        /*this.Dispatcher.InvokeAsync(() => {
                            WriteLineToMessageLog("upper");
                        });*/
                        return index;
                    }
                    else
                        return -1;
            };

            TimeSpan medianTimeElapsedBetweenFrames =
                TimeSpan.FromMilliseconds(medianTimeElapsedBetweenFrames_milliseconds);

            skeletonPlayback_Task = Task.Run(() =>
            {
                // playback (including pausing) continues until it is cancelled
                while (!isCancelled)
                {
                    int index = Get_SkeletonFrameIndex();

                    Thread.Sleep(millisecondsBetweenAviFrameRates_ts);
                    if (index != -1)
                        UpdateSkeletonReplay(index);

                    // If playback is paused, then update skeleton replay one more time and then
                    // wait until playback is unpaused
                    if (isPaused)
                    {
                        index = Get_SkeletonFrameIndex();
                        UpdateSkeletonReplay(index);

                        positionChangeEvent.WaitOne();
                    }
                }
            });
        }

        void UpdateSkeletonReplay(int index)
        {
            SkeletonReplayData frame = skeletonFrames[index];

            skeletonFrameWindow.Dispatcher.InvokeAsync(() =>
            {
                Skeleton[] skeletons = { frame.skeleton };
                skeletonFrameWindow.painter.Draw(skeletons);
            });
        }
        //---//
        

        //-- Playback ---//
        bool isMediaLoaded = false;
        bool isPaused = false;
        AutoResetEvent positionChangeEvent = new AutoResetEvent(false);
        
        void PlayPauseButton_Click(object sender, EventArgs e)
        {
            if (isMediaLoaded) 
            {
                if (!isPaused) // if is playing
                {
                    // then pause
                    isPaused = true;
                }
                else // if is paused
                {
                    // then play
                    positionChangeEvent.Set();
                    isPaused = false;
                }
            }
        }

        void PositionChanged(object sender, TimeSpan ts)
        {
            if (isPaused)
                positionChangeEvent.Set();
        }

        void StopButton_Click(object sender, EventArgs e)
        {
            if (!isPaused)
            {
                PlayPauseButton_Click(sender, e);
            }
            else
            {
                positionChangeEvent.Set();
            }
        }
        //--- ---//        

        
        void WindowClosing(object sender, CancelEventArgs e)
        {
            /*Clean();

            skeletonFrameWindow.Close();
            aviWindow.Close();*/
        }

        public void WriteLineToMessageLog(object textToAppend)
        {
            this.messageLog.AppendText(DateTime.Now.ToString("HH:mm:ss") + " - " + textToAppend + Environment.NewLine);
            this.messageLog.ScrollToEnd();
        }


        //--- Methods for locking/unlocking windows to move them together/separately ---//
        bool isWindowsLocked = true;

        private void WindowLocationChanged(object sender, EventArgs e)
        {
            // if the windows are locked, then move them together
            if (isWindowsLocked)
            {
                SetSubWindowLocations();
            }
        }

        private void SetSubWindowLocations()
        {
            skeletonFrameWindow.Left = this.Left + this.Width;
            skeletonFrameWindow.Top = this.Top;
            
            aviWindow.Left = this.Left;
            aviWindow.Top = this.Top + this.Height;
        }

        private void LockUnlockWindows_Click(object sender, RoutedEventArgs e)
        {
            // if windows are locked, then unlock them
            if (isWindowsLocked)
            {
                RemoveLocationChangedEvents();
                
                this.lockOption.Content = "Lock Windows";
                isWindowsLocked = false;
            }
            // if windows are not locked, then lock them
            else
            {
                AddLocationChangedEvents();

                this.lockOption.Content = "Unlock Windows";
                isWindowsLocked = true;
            }
        }

        private void ResetWindows_Click(object sender, RoutedEventArgs e)
        {
            InitializeWindowSizesAndPositions();

            if (!isWindowsLocked)
                LockUnlockWindows_Click(sender, e);
        }
        //--- ---//
    }
}
