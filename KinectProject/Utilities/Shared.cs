﻿// default namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// additional namespaces
using System.Windows;
using Microsoft.Kinect;
using System.Threading;


namespace Shared
{
    public static class Utilities
    {
        public static void UpdateWindowFromDifferentThread_Synchronously(this Window window, Action action)
        {
            // http://stackoverflow.com/a/9732853
            window.Dispatcher.Invoke((Action)(() =>
            {
                action();
            }));
        }

        public static void UpdateUIfromDifferentThread_Asynchronously(this Window window, Action action)
        {
            // http://stackoverflow.com/a/9732853
            window.Dispatcher.BeginInvoke((Action)(() =>
            {
                action();
            }));
        }
        
        public static string AddQuotationsToString(string _string)
        {
            return "\"" + _string + "\"";
        }

        public static string AddSingleQuotationsToString(string _string)
        {
            return "'" + _string + "'";
        }

        public static String GetFullPathWithoutExtension(String path)
        {
            return System.IO.Path.Combine(System.IO.Path.GetDirectoryName(path), System.IO.Path.GetFileNameWithoutExtension(path));
        }
    }


    /* Creates a separate Window on a background thread
     * This enables UI on separate threads
     */
    public class ThreadedWindowHandler<WINDOW_TYPE> where WINDOW_TYPE : Window
    {
        // How do I create and show WPF windows on separate threads?
        // http://stackoverflow.com/a/1111485
   
        private WINDOW_TYPE window;

        public ThreadedWindowHandler(object arg1)
        {
            Thread windowThread = new Thread(() =>
            {
                // Passing arguments to C# generic new() of templated type:
                // http://stackoverflow.com/a/5598999
                window = (WINDOW_TYPE)Activator.CreateInstance(typeof(WINDOW_TYPE), arg1);
                window.Show();
                System.Windows.Threading.Dispatcher.Run();
            });

            windowThread.SetApartmentState(ApartmentState.STA);
            windowThread.IsBackground = true;
            windowThread.Start();
            
            // wait until window is initialized
            while (window == null || !window.IsInitialized)
                Thread.Sleep(100);
        }

        public WINDOW_TYPE Window
        {
            get
            {
                return this.window;
            }
        }
    }


    // Use WindowClosing to override Window.Close event of a Window 
    public static class WindowsCloseButton
    {
        // Centrally disable/enable the closing of overridden windows
        public static void DisableWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
