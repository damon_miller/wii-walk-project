﻿// default namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// additional namespaces
using Shared;
using System.IO;


namespace KinectRecord
{
    // used to allow manual input of recording information about the recording session
    public partial class MetadataWindow : Window
    {
        public MetadataWindow()
        {
            InitializeComponent();

            this.startFinish_Button.Content = "Start Kinect";
            this.Loaded += WindowLoaded_BeforeRecordingSession;
            this.startFinish_Button.Click += StartButton_Click;
        }

        string filename;

        public MetadataWindow(string filename)
        {
            InitializeComponent();

            this.startFinish_Button.Content = "Exit Session";
            this.Loaded += WindowLoaded_AfterRecordingSession;
            this.startFinish_Button.Click += FinishButton_Click;
            this.Closing += WindowsCloseButton.DisableWindowClosing;

            this.filename = filename;
        }

        void WindowLoaded_BeforeRecordingSession(object sender, EventArgs e)
        {
            metadataBox.Text +=
                "Enter any information about upcoming recording session in text box."
                + Environment.NewLine
                + "Then click " + Utilities.AddQuotationsToString((string)startFinish_Button.Content) + " to start recording.";

            metadataBox.GotFocus += removeIntroText_BeforeRecordingSession;
        }

        const int TYPING_OFFSET = 50;

        void removeIntroText_BeforeRecordingSession(object sender, EventArgs e)
        {
            metadataBox.Clear();
            metadataBox.GotFocus -= removeIntroText_BeforeRecordingSession;

            addMetaDataTemplateText();
            ResizeWindowToContent();
        }

        void ResizeWindowToContent()
        {
            this.SizeToContent = System.Windows.SizeToContent.WidthAndHeight;
            this.SizeToContent = System.Windows.SizeToContent.Manual;
            this.Width += TYPING_OFFSET;
        }

        readonly String templateFilePath =
            "..\\..\\" + "MetaDataTemplate.txt";

        void addMetaDataTemplateText()
        {
            metadataBox.Text = File.ReadAllText(templateFilePath);
            metadataBox.Text += Environment.NewLine;
        }

        void WindowLoaded_AfterRecordingSession(object sender, EventArgs e)
        {
            metadataBox.Text +=
                 "Enter any information about finished recording session in text box."
                 + Environment.NewLine
                 + "Then click " + Utilities.AddQuotationsToString((string)startFinish_Button.Content) + " to start recording.";

            metadataBox.GotFocus += removeIntroText_AfterRecordingSession;
        }

        const string EXTENSION = ".txt";

        void removeIntroText_AfterRecordingSession(object sender, EventArgs e)
        {
            metadataBox.Clear();
            metadataBox.GotFocus -= removeIntroText_AfterRecordingSession;

            metadataBox.Text = File.ReadAllText(filename + EXTENSION);
            ResizeWindowToContent();
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            // ensure that Textbox was focused at least once; in order to remove
            // remove introductory text in Textbox
            metadataBox.Focus();

            DateTime thisDay = DateTime.UtcNow;
            filename = thisDay.ToString("yyyy.MM.dd_HH.mm.ss");
            Directory.CreateDirectory(filename);
            filename += "\\" + filename;

            SaveTextBox();

            // initialize main recording session window and close pre-recording
            // session window
            MainWindow mainWindow = new MainWindow(filename);
            this.Close();
            mainWindow.Show();
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            // ensure that Textbox was focused at least once; in order to remove
            // introductory text in Textbox
            metadataBox.Focus();

            SaveTextBox();

            // closes post-recording session window and closes program
            this.Closing -= WindowsCloseButton.DisableWindowClosing;
            this.Close();
        }

        // saves text inputed in Textbox to text file
        void SaveTextBox()
        {
            if (!metadataBox.Text.EndsWith(Environment.NewLine))
            {
                metadataBox.Text += Environment.NewLine;
            }

            // http://stackoverflow.com/a/1225869/3781601
            File.WriteAllText(filename + EXTENSION, metadataBox.Text);
        }
    }
}
