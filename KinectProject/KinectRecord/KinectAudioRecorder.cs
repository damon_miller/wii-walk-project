﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace KinectRecord
{
    // adapted from KinectforWindowsSDKV1 -> 6.Audio -> AudioRecorder
    // http://channel9.msdn.com/Series/KinectQuickstart/Audio-Fundamentals: "Quickstart slides and samples"
    public class KinectAudioRecorder
    {
        // http://msdn.microsoft.com/en-us/library/windows/hardware/ff538799(v=vs.85).aspx
        struct WAVEFORMATEX
        {
            public ushort wFormatTag;
            public ushort nChannels;
            public uint nSamplesPerSec;
            public uint nAvgBytesPerSec;
            public ushort nBlockAlign;
            public ushort wBitsPerSample;
            public ushort cbSize;
        }


        public string filePath;
        private BinaryWriter binaryWriter;
        int cbFormat = 18; //sizeof(WAVEFORMATEX)

        // WAVE PCM soundfile format: https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
        int dataLength = 0;  // number of bytes of written audio data
        int dataLength_Pos1;  // position after RIFF
        int dataLength_Pos2;  // position after Subchunk2ID

        public KinectAudioRecorder(string _filePath)
        {
            this.filePath = _filePath;
            Stream fileStream = File.Create(filePath);
            binaryWriter = new BinaryWriter(fileStream);
                
            //--- RIFF header
            //binaryWriter.Write("RIFF");
            WriteString(fileStream, "RIFF");
            dataLength_Pos1 = (int)binaryWriter.BaseStream.Position;
            binaryWriter.Write(dataLength + cbFormat + 4); //File size - 8
            //binaryWriter.Write("WAVE");
            WriteString(fileStream, "WAVE");

            //--- WAVEFORMATEX
            WAVEFORMATEX format = new WAVEFORMATEX()
            {
                // KinectAudioSource format: http://msdn.microsoft.com/en-us/library/hh855697.aspx
                wFormatTag = 1, // PCM
                nChannels = 1, // Mono = 1
                nSamplesPerSec = 16000, // 16 kHz
                nAvgBytesPerSec = 32000, // == SampleRate * NumChannels * BitsPerSample/8
                nBlockAlign = 2, // == NumChannels * BitsPerSample/8
                wBitsPerSample = 16, // 16 bits
                cbSize = 0 // implicitly zero for PCM: http://msdn.microsoft.com/en-us/library/windows/hardware/ff538799(v=vs.85).aspx
            };
            //binaryWriter.Write("fmt ");
            WriteString(fileStream, "fmt ");
            binaryWriter.Write(cbFormat);
            binaryWriter.Write(format.wFormatTag);
            binaryWriter.Write(format.nChannels);
            binaryWriter.Write(format.nSamplesPerSec);
            binaryWriter.Write(format.nAvgBytesPerSec);
            binaryWriter.Write(format.nBlockAlign);
            binaryWriter.Write(format.wBitsPerSample);
            binaryWriter.Write(format.cbSize);

            //data header
            //binaryWriter.Write("data");
            WriteString(fileStream, "data");
            dataLength_Pos2 = (int)binaryWriter.BaseStream.Position;
            binaryWriter.Write(dataLength);
        }

        // write string to stream without the string's null terminating character
        static void WriteString(Stream stream, string s)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(s);
            stream.Write(bytes, 0, bytes.Length);
        }

        public void Write(byte[] buffer, int count)
        {
            binaryWriter.Write(buffer, 0, count);
            dataLength += count;
        }

        public void Close()
        {
            UpdateHeader();

            binaryWriter.Close();
        }

        void UpdateHeader()
        {
            binaryWriter.Seek(dataLength_Pos1, SeekOrigin.Begin);
            binaryWriter.Write(dataLength + cbFormat + 4);

            binaryWriter.Seek(dataLength_Pos2, SeekOrigin.Begin);
            binaryWriter.Write(dataLength);

            binaryWriter.Seek(0, SeekOrigin.End);
        }
    }
}
