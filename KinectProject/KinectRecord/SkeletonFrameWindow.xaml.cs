﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Microsoft.Kinect;
using Shared;
using System.Threading;

namespace KinectRecord
{
    public partial class SkeletonFrameWindow : Window
    {
        MainWindow mainWindow;

        public SkeletonFrameWindow(MainWindow mainWindow)
        {
            InitializeComponent();
            
            this.mainWindow = mainWindow;
        }

       public void UpdateLocationOfMainWindow(object sender, EventArgs e)
        {
            if (this.IsActive)
            {
                double mainWindow_Left = this.Left - this.Width;
                double mainWindow_Top = this.Top;
                    
                mainWindow.Dispatcher.BeginInvoke((Action)(() =>
                {
                    mainWindow.LocationChanged -= mainWindow.SkeletonFrameWindow_UpdateLocation;
                    mainWindow.Left = mainWindow_Left;
                    mainWindow.Top = mainWindow_Top;
                    mainWindow.LocationChanged += mainWindow.SkeletonFrameWindow_UpdateLocation;
                }));
            }
        }

       public void AddLocationChangedEvents()
       {
           this.LocationChanged += UpdateLocationOfMainWindow;
       }

       public void RemoveLocationChangedEvents()
       {
           this.LocationChanged -= UpdateLocationOfMainWindow;
       }

    }
}
