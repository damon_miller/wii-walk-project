﻿using System.IO;
using Microsoft.Kinect;
using System.Runtime.Serialization.Formatters.Binary;
using Shared;

using System.IO.Compression;
using System.Collections.Generic;


namespace KinectRecord
{
    public class SkeletonRecorder
    {
        private Stream recordStream;
        private BinaryWriter writer;

        string filePath;            

        public SkeletonRecorder(string filename)
        {
            this.filePath = filename;

            recordStream = File.Create(filename);
            writer = new BinaryWriter(recordStream);
        }

        public void Close()
        {
            writer.Close();
            writer = null;
            
            recordStream.Close();
            recordStream = null;

            CompressFile(filePath);
        }

        // write skeleton data to file
        public void Record(SkeletonReplayData skeletonReplayData)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(writer.BaseStream, skeletonReplayData);
        }

        // GZipStream: http://msdn.microsoft.com/en-us/library/system.io.compression.gzipstream(v=vs.110).aspx
        public static void CompressFile(string fileName)
        {
            FileInfo fileToCompress = new FileInfo(fileName);
            using (FileStream originalFileStream = fileToCompress.OpenRead())
            {
                if ((File.GetAttributes(fileToCompress.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                {
                    string compressedFileName = fileToCompress + ".gz";

                    using (FileStream compressedFileStream = File.Create(compressedFileName))
                    {
                        using (GZipStream compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                        {
                            originalFileStream.CopyTo(compressionStream);
                        }
                    }
                }
            }
            File.Delete(fileName);
        }
    }


    [System.Serializable]
    public class SkeletonReplayData
    {
        public SkeletonReplayData(double millisecondsElapsedSinceLastFrame, Skeleton skeleton)
        {
            this.millisecondsElapsedSinceLastFrame = millisecondsElapsedSinceLastFrame;
            this.skeleton = skeleton;
        }

        public double millisecondsElapsedSinceLastFrame;
        public Skeleton skeleton;
    }
}
