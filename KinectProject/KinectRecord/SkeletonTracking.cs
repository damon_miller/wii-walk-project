﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Kinect;

namespace KinectRecord
{
    class SkeletonTracking
    {
        public int skeletonIndex = 0;
        bool skeletonIndexInitialized = false;
        public bool isSkeletonTracked = false;

        public void UpdateWhetherSkeletonIsTracked(Skeleton[] skeletons)
        {
            bool isNoLongerTrackingSkeleton =
                //skeletonIndex == -1 
                !skeletonIndexInitialized
                ||
                skeletons[skeletonIndex].TrackingState != SkeletonTrackingState.Tracked;

            if (isNoLongerTrackingSkeleton)
            {
                isSkeletonTracked = IsSkeletonTracked(skeletons);
            }
        }
        
        private bool IsSkeletonTracked(Skeleton[] skeletons)
        {
            // If
            for (int i = 0; i < skeletons.Length; i++)
            {
                if (skeletons[i].TrackingState == SkeletonTrackingState.Tracked)
                {
                    skeletonIndex = i;

                    if (!skeletonIndexInitialized)
                        skeletonIndexInitialized = true;
                    
                    return true;
                }
            }

            // Else
            return false;
        }
    }
}
