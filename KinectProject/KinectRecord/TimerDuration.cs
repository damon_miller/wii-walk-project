﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectRecord
{
    class TimerDuration
    {
        DateTime baseDateTime;
        DateTime pauseStart;
        DateTime pauseFinish;
        TimeSpan pauseDuration = TimeSpan.Zero;
        bool isStarted = false;

        public TimerDuration()
        {
            //baseDateTime = DateTime.Now;
        }

        public void Start()
        {
            baseDateTime = DateTime.Now;
            isStarted = true;
        }

        public void Pause()
        {
            pauseStart = DateTime.Now;
        }

        public void Unpause()
        {
            pauseFinish = DateTime.Now;

            pauseDuration += pauseFinish - pauseStart;
        }

        public TimeSpan Duration
        {
            get 
            {
                if (!isStarted)
                    return TimeSpan.Zero;
                else
                    return (DateTime.Now - baseDateTime) - pauseDuration;
            }
        }
    }
}
