﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.IO;
using Shared;
using System.Threading;

namespace KinectRecord
{
    /* Uses the FFmpeg library: https://www.ffmpeg.org/
     * NOTE: methods require ffmpeg.exe or ffprobe.exe
     * So ensure that these executables are in the same directory as the application
     */
    public class FFmpeg
    {
        public FFmpeg()
        {
            string ffmpeg = "ffmpeg.exe";
            string ffprobe = "ffprobe.exe";

            // check to make sure files exist, otherwise cannot proceed
            bool file1Exists = File.Exists(ffmpeg);
            bool file2Exists = File.Exists(ffprobe);

            if ( !file1Exists || !file2Exists )
                throw new Exception(ffmpeg + " or " + ffprobe + " is missing in root folder of program.");
        }
        
        public double GetFileDuration_ffprobe(string filePath)
        {
            Process commandLine = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "cmd.exe",

                    // .NET - WindowStyle = hidden vs. CreateNoWindow = true?
                    // http://stackoverflow.com/a/5094208
                    CreateNoWindow = true,
                    UseShellExecute = false,

                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                }
            };
            commandLine.Start();

            string argument = "ffprobe -i " + filePath + @" -show_entries format=duration -v quiet -of csv=""p=0""";
            
            commandLine.StandardInput.WriteLine(argument);
            
            while (!commandLine.StandardOutput.EndOfStream)
            {
                string line = commandLine.StandardOutput.ReadLine();

                if (line.Contains("ffprobe"))
                    break;
            }

            string line2 = commandLine.StandardOutput.ReadLine();
            
            commandLine.StandardInput.WriteLine("exit");
            commandLine.WaitForExit();

            double duration = Convert.ToDouble(line2);

            return duration;
        }

        public void MergeWavWithAvi(string filePath_avi, string filePath_wav)
        {
            string directory = System.IO.Path.GetDirectoryName(filePath_avi);
            string extension = System.IO.Path.GetExtension(filePath_avi);
            string tempFilePath = directory + "\\" + "temp" + extension;

            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                // [FFmpeg-user] Adding Audio to a video without re-encoding?: 
                // http://ffmpeg.org/pipermail/ffmpeg-user/2015-January/025079.html
                // ffmpeg -i video.mp4 -i audio.m4a -c copy -map 0:v -map 1:a output.mp4
                FileName = "ffmpeg",
                Arguments = " -i " + filePath_avi + " -i " + filePath_wav + " -c copy -map 0:v -map 1:a " + tempFilePath,
                
                CreateNoWindow = true,
                UseShellExecute = false,
            };

            // execute merging of AVI file and WAV file
            Process process = Process.Start(processStartInfo);
            process.WaitForExit();  // wait till re-encoding process complete

            File.Delete(filePath_avi);
            File.Delete(filePath_wav);

            // rename temporary file to name of the AVI file
            File.Move(tempFilePath, filePath_avi);
        }
    }
}
