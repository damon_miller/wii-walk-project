﻿// default namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// additional namespaces
using Microsoft.Kinect;
using Shared;


namespace KinectRecord
{
    public partial class ColorFrameWindow : Window
    {
        public WriteableBitmap colorBitmap;

        MainWindow mainWindow;

        public ColorFrameWindow(MainWindow mainWindow) 
        {
            InitializeComponent();

            this.mainWindow = mainWindow;
        }

        public void InitializeImage(KinectSensor _kinectSensor)
        {
            // This is the bitmap we'll display on-screen
            this.colorBitmap = new WriteableBitmap(_kinectSensor.ColorStream.FrameWidth, _kinectSensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);

            // Set the image we display to point to the bitmap where we'll put the image data
            this.Image.Source = this.colorBitmap;
        }

        public void UpdateImage(byte[] colorPixels)
        {
            this.colorBitmap.WritePixels(
                new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                colorPixels,
                this.colorBitmap.PixelWidth * sizeof(int),
                0);
        }

        public void UpdateLocationOfMainWindow(object sender, EventArgs e)
        {
            if (this.IsActive)
            {
                double thisLeft = this.Left;
                double thisTop = this.Top; ;

                mainWindow.Dispatcher.BeginInvoke((Action)(() =>
                {
                    mainWindow.LocationChanged -= mainWindow.ColorFrameWindow_UpdateLocation;
                    mainWindow.Left = thisLeft;
                    mainWindow.Top = thisTop - mainWindow.Height;
                    mainWindow.LocationChanged += mainWindow.ColorFrameWindow_UpdateLocation;
                }));
            }
        }
        
        public void AddLocationChangedEvents()
        {
            this.LocationChanged += UpdateLocationOfMainWindow;
        }

        public void RemoveLocationChangedEvents()
        {
            this.LocationChanged -= UpdateLocationOfMainWindow;
        }
    }
}
