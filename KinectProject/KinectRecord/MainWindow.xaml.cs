﻿// default namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

// additional namespaces
using Microsoft.Kinect;
using System.IO;
using Kinect2BVH;
using SharpAvi;
using SharpAvi.Output;
using SharpAvi.Codecs;
using Shared;
using System.Text.RegularExpressions;


namespace KinectRecord
{
    public partial class MainWindow : Window
    {
        readonly string filePath_base;

        public MainWindow(string filePath)
        {
            InitializeComponent();

            // get base filepath from MetadataWindow
            this.filePath_base = filePath;

            // disable Window close button
            this.Closing += WindowsCloseButton.DisableWindowClosing;
        }

        /* adapted from Kinect Toolbox: https://kinecttoolbox.codeplex.com/:
         * -> KinectToolbox\GesturesViewer\MainWindow.xaml.cs
         */
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            InitializeBackgroundWindows();

            try
            {
                // listen for any changes to status of Kinect
                KinectSensor.KinectSensors.StatusChanged += Kinects_StatusChanged;

                // get first Kinect Sensor
                kinectSensor = KinectSensor.KinectSensors.First();

                // check to make sure Kinect is properly connected to computer
                if (kinectSensor.Status == KinectStatus.Connected)
                {
                    // startup the Kinect
                    InitializeKinect();

                    // Initialize streaming of audio data by polling for it in a 
                    // separate thread
                    // Using max value of threshold so that no audio packets are dropped
                    TimeSpan readStaleThreshold = TimeSpan.MaxValue;
                    audioStream = kinectSensor.AudioSource.Start(readStaleThreshold);
                    audioPolling_Task = Task.Run(() => AudioStreamReader());
                }

                WriteLineToMessageLog("Kinect status: " + kinectSensor.Status);
            }
            catch (Exception ex)
            {
                WriteLineToMessageLog("Kinect initialization error: " + ex.Message);
                WriteLineToMessageLog("Check that Kinect's USB and power adapter are plugged in");
            }

            // starts off this window as the selected window (just for cosmetic reasons)
            this.Activate();
        }


        //--- Layout of Windows ---//
        SkeletonFrameWindow skeletonFrameWindow;
        ColorFrameWindow colorFrameWindow;

        void InitializeBackgroundWindows()
        {
            // open SkeletonFrame window
            var skeletonFrameWindowHandler = new ThreadedWindowHandler<SkeletonFrameWindow>(this);

            // open ColorFrame window
            var colorFrameWindowHandler = new ThreadedWindowHandler<ColorFrameWindow>(this);

            skeletonFrameWindow = skeletonFrameWindowHandler.Window;

            skeletonFrameWindow.UpdateWindowFromDifferentThread_Synchronously(() =>
            {
                skeletonFrameWindow.Closing += WindowsCloseButton.DisableWindowClosing;
            });

            colorFrameWindow = colorFrameWindowHandler.Window;

            colorFrameWindow.UpdateWindowFromDifferentThread_Synchronously(() =>
            {
                colorFrameWindow.Closing += WindowsCloseButton.DisableWindowClosing;
            });

            InitializeWindowSizesAndPositions();

            AddLocationChangedEvents();
        }

        void CloseBackgroundWindows()
        {
            colorFrameWindow.UpdateWindowFromDifferentThread_Synchronously(() =>
            {
                colorFrameWindow.Closing -= WindowsCloseButton.DisableWindowClosing;
                colorFrameWindow.Close();
            });

            skeletonFrameWindow.UpdateWindowFromDifferentThread_Synchronously(() =>
            {
                skeletonFrameWindow.Closing -= WindowsCloseButton.DisableWindowClosing;
                skeletonFrameWindow.Close();
            });
        }

        void InitializeWindowSizesAndPositions()
        {
            object colorFrameWindow_Width = null;
            object colorFrameWindow_Height = null;

            // invoke background window's dispatcher b/c accessing state of background window from 
            // the main thread (which is a different thread)
            colorFrameWindow.Dispatcher.Invoke(() =>
            {
                colorFrameWindow_Width = colorFrameWindow.Width;
                colorFrameWindow_Height = colorFrameWindow.Height;
            });

            double mainWindow_Width = this.Width;
            double mainWindow_Height = this.Height;

            skeletonFrameWindow.Dispatcher.Invoke(() =>
            {
                skeletonFrameWindow.Width = mainWindow_Width;
                skeletonFrameWindow.Height = mainWindow_Height + (double)colorFrameWindow_Height;
            });

            SkeletonFrameWindow_UpdateLocation();
            ColorFrameWindow_UpdateLocation();
        }

        void AddLocationChangedEvents()
        {
            this.LocationChanged += ColorFrameWindow_UpdateLocation;
            this.LocationChanged += SkeletonFrameWindow_UpdateLocation;

            colorFrameWindow.AddLocationChangedEvents();
            skeletonFrameWindow.AddLocationChangedEvents();
        }

        void RemoveLocationChangedEvents()
        {
            this.LocationChanged -= ColorFrameWindow_UpdateLocation;
            this.LocationChanged -= SkeletonFrameWindow_UpdateLocation;

            colorFrameWindow.RemoveLocationChangedEvents();
            skeletonFrameWindow.RemoveLocationChangedEvents();
        }
        //--- ---//


        //--- Methods for moving windows in sync ---//
        public void SkeletonFrameWindow_UpdateLocation(object sender, EventArgs e)
        {
            SkeletonFrameWindow_UpdateLocation();
        }

        void SkeletonFrameWindow_UpdateLocation()
        {
            double leftPosition = this.Left + this.Width;
            double topPosition = this.Top;

            skeletonFrameWindow.Dispatcher.Invoke(() =>
            {
                skeletonFrameWindow.Left = leftPosition;
                skeletonFrameWindow.Top = topPosition;
            });
        }

        public void ColorFrameWindow_UpdateLocation(object sender, EventArgs e)
        {
            ColorFrameWindow_UpdateLocation();
        }

        void ColorFrameWindow_UpdateLocation()
        {
            double leftPosition = this.Left;
            double topPosition = this.Top + this.Height;

            colorFrameWindow.Dispatcher.Invoke(() =>
            {
                colorFrameWindow.Left = leftPosition;
                colorFrameWindow.Top = topPosition;
            });
        }
        //--- ---//


        //--- Reset and Lock/Unlock Window functionality ---//
        private void ResetWindows_Click(object sender, RoutedEventArgs e)
        {
            InitializeWindowSizesAndPositions();

            if (!isWindowsLocked)
                LockUnlockWindows_Click(sender, e);
        }

        bool isWindowsLocked = true;

        private void LockUnlockWindows_Click(object sender, RoutedEventArgs e)
        {
            if (isWindowsLocked)
            {
                RemoveLocationChangedEvents();

                isWindowsLocked = false;
                this.lockUnlockWindows_Button.Content = "Lock Windows";
            }
            else
            {
                AddLocationChangedEvents();

                isWindowsLocked = true;
                this.lockUnlockWindows_Button.Content = "Unlock Windows";
            }
        }
        //--- ---//


        private KinectSensor kinectSensor;
        private Stream audioStream;
        Task audioPolling_Task;

        // Adapted from Kinect Toolbox: https://kinecttoolbox.codeplex.com/
        void Kinects_StatusChanged(object sender, StatusChangedEventArgs e)
        {
            //  if Kinect has become connected, then 
            //      if Kinect is not initialized, then initialize it
            if (e.Status == KinectStatus.Connected)
            {
                if (kinectSensor == null)
                {
                    kinectSensor = e.Sensor;
                    InitializeKinect();
                }
            }
            // if Kinect is initialized, then close it
            else
            {
                if (kinectSensor == e.Sensor)
                {
                    this.kinectSensor.Dispose();
                    kinectSensor = null;
                }
            }

            WriteLineToMessageLog("Kinect status: " + e.Status.ToString());
        }

        public void WriteLineToMessageLog(object textToAppend)
        {
            this.messageLog.AppendText(DateTime.Now.ToString("HH:mm:ss") + " - " + textToAppend + Environment.NewLine);
            this.messageLog.ScrollToEnd();
        }



        const ColorImageFormat COLOR_IMAGE_FORMAT = ColorImageFormat.RgbResolution640x480Fps30;
        const int FRAME_WIDTH = 640;
        const int FRAME_HEIGHT = 480;
        public const int FRAMES_PER_SECOND = 30;
        public static readonly double durationBetweenFrames = Math.Pow(10, 3) * (1 / (double)FRAMES_PER_SECOND);

        public void InitializeKinect()
        {
            // Turn on the skeleton stream to receive skeleton frames
            this.kinectSensor.SkeletonStream.Enable();

            // Add an event handler to be called whenever there is new color frame data
            this.kinectSensor.SkeletonFrameReady += this.SkeletonFrameReady;

            
// C# Debug and release mode: http://stackoverflow.com/a/19053995
#if DEBUG
            // Uncomment for debugging, otherwise keep commented
            // Uses Seated Mode for skeleton tracking (easier for testing skeleton capture while 
            // sitting down)
            this.kinectSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
#else
            this.kinectSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
#endif

            // Turn on the color stream to receive color frames
            this.kinectSensor.ColorStream.Enable(COLOR_IMAGE_FORMAT);

            // Add an event handler to be called whenever there is new color frame data
            this.kinectSensor.ColorFrameReady += this.ColorFrameReady;

            // set image of colorFrameWindow to specifications of Kinect ColorStream data
            colorFrameWindow.Dispatcher.Invoke(() =>
            {
                colorFrameWindow.InitializeImage(kinectSensor);
            });


            // Start the sensor!
            try
            {
                this.kinectSensor.Start();
            }
            catch (IOException error)
            {
                WriteLineToMessageLog(error.Message);
            }
        }


        bool haveFirstColorImageFrame = false;
        double firstColorImageFrameTimestamp;
        double lastColorImageFrameTimestamp;

        // processes color frames pushed from Kinect
        void ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame colorImageFrame = e.OpenColorImageFrame())
            {
                if (colorImageFrame != null)
                {
                    // get colour pixel data from color frame
                    byte[] data = new byte[colorImageFrame.PixelDataLength];
                    colorImageFrame.CopyPixelDataTo(data);

                    // update colorFrameWindow's image
                    colorFrameWindow.Dispatcher.InvokeAsync(() =>
                    {
                        colorFrameWindow.UpdateImage(data);
                    });

                    // If is recording and skeleton is tracked, 
                    // then add recording of RGB data to recording tasks
                    if (recordingStatus == RecordingStatus.IsRecording && skeletonTracking.isSkeletonTracked)
                    {
                        // if is first color image frame to be recorded
                        if (!haveFirstColorImageFrame)
                        {
                            // save time stamp of first color image frame
                            firstColorImageFrameTimestamp = colorImageFrame.Timestamp;
                            haveFirstColorImageFrame = true;
                        }
                        // save time stamp of most recent color image frame
                        lastColorImageFrameTimestamp = colorImageFrame.Timestamp;

                        recording_Task = recording_Task.ContinueWith((Task t) =>
                        {
                            encodingStream.WriteFrame(true, data, 0, data.Length);                                
                        });
                    }
                    else
                    {
                        // since not recording or lost track of skeleton, 
                        // do not have the first color image frame for the next recording
                        haveFirstColorImageFrame = false;
                    }
                }
            }
        }


        SkeletonTracking skeletonTracking = new SkeletonTracking();
        bool isFirstSkeletonFrame = true;
        double previousTimestamp;
        double currentTimestamp = durationBetweenFrames;

        // processes skeleton frames pushed from Kinect
        void SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    // get skeleton data
                    Skeleton[] skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);

                    // checks to see if there is a skeleton being tracked, and subsequently 
                    // updates the skeletonIndex
                    skeletonTracking.UpdateWhetherSkeletonIsTracked(skeletons);

                    Skeleton skeleton = skeletons[skeletonTracking.skeletonIndex];
                    // flip x-coordinates of skeleton joints
                    skeleton.Mirror();
                    
                    // update skeleton in skeletonFrameWindow
                    skeletonFrameWindow.Dispatcher.InvokeAsync(() =>
                    {
                        Skeleton[] skeletonsAdjusted = { skeleton };
                        skeletonFrameWindow.painter.Draw(skeletonsAdjusted);
                    });

                    // if (have not recorded first color image frame OR skeleton frame is earlier than color frame), 
                    // then do not consider skeleton frame
                    if (!haveFirstColorImageFrame || firstColorImageFrameTimestamp > skeletonFrame.Timestamp)
                        return;

                    //  If there is a skeleton being tracked, then 
                    //      if is recording, then add recording of skeleton to recording tasks
                    //      (which records the skeleton data to BVH file and GZ file)                    
                    if (recordingStatus == RecordingStatus.IsRecording && skeletonTracking.isSkeletonTracked)
                    {
                        recording_Task = recording_Task.ContinueWith((Task t) =>
                        {
                            Skeleton[] skeletons2 = new Skeleton[skeletonFrame.SkeletonArrayLength];
                            skeletonFrame.CopySkeletonDataTo(skeletons2);
                            Skeleton skeleton_nonMirrored = skeletons2[skeletonTracking.skeletonIndex];

                            WriteEntryToBVH_File(skeleton_nonMirrored);
                        });

                        if (isFirstSkeletonFrame)
                        {
                            previousTimestamp = firstColorImageFrameTimestamp;
                            isFirstSkeletonFrame = false;
                        }
                        currentTimestamp += skeletonFrame.Timestamp - previousTimestamp;
                        previousTimestamp = skeletonFrame.Timestamp;
                            
                        SkeletonReplayData data =
                            new SkeletonReplayData(currentTimestamp, skeleton);

                        recording_Task = recording_Task.ContinueWith((Task t) =>
                        {
                            skeletonRecorder.Record(data);
                        });
                    }
                    else
                    {
                        isFirstSkeletonFrame = true;
                    }
                }
            }
        }

        // Writes skeleton data to a BVH file
        // Adapted from sensor_allFramesReady (see packages\Kinect2BVHSVN\erstesKinectProjekt\Form1.cs)
        private void WriteEntryToBVH_File(Skeleton skeleton)
        {
            if (skeleton.TrackingState == SkeletonTrackingState.Tracked)
            {
                if (bvhFile != null)
                {
                    if (bvhFile.isRecording == true && bvhFile.isInitializing == true)
                    {
                        bvhFile.Entry(skeleton);

                        if (bvhFile.intializingCounter > initFrames)
                        {
                            bvhFile.startWritingEntry();
                        }
                    }

                    if (bvhFile.isRecording == true && bvhFile.isInitializing == false)
                    {
                        bvhFile.Motion(skeleton);
                    }
                }
            }
        }

        
        enum RecordingStatus { NotRecording, IsRecording, PausedRecording };
        RecordingStatus recordingStatus = RecordingStatus.NotRecording;

        // initialize a recording task (which will be later added to with additional recording tasks)
        Task recording_Task = Task.Run(() => { });

        string filePath_wav;
        string filePath_avi;

        private SkeletonRecorder skeletonRecorder;

        private writeBVH bvhFile;
        int initFrames = 1;

        AviWriter aviWriter;
        IAviVideoStream encodingStream;


        /* First hit of record button initializes the recording of the RGB data and skeleton data
        // to AVI, BVH, and GZ files
         * Subsequent hits toggles pausing/unpausing of recording
         */
        async void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            switch (recordingStatus)
            {
                // if not recording, then record
                case RecordingStatus.NotRecording:
                {
                    recording_Task = recording_Task.ContinueWith((continuation) =>
                    {
                        skeletonRecorder = new SkeletonRecorder(filePath_base);

                        bvhFile = new writeBVH(filePath_base);

                        AviRecorder_initialize(filePath_base);
                        WavRecorder_initialize(filePath_base);
                    });
                    await recording_Task; // wait until this recording task is completed

                    recordingStatus = RecordingStatus.IsRecording;
                        
                    var directory = System.IO.Path.GetDirectoryName(filePath_base);
                    WriteLineToMessageLog("Recording data to folder " + Utilities.AddQuotationsToString(directory));
                    record_Button.Content = "Pause Recording";


                    break;
                }
                // if recording, then pause
                case RecordingStatus.IsRecording: 
                {
                    recordingStatus = RecordingStatus.PausedRecording;
                    
                    WriteLineToMessageLog("Paused recording");
                    record_Button.Content = "Resume recording";


                    break;
                }
                // if recording paused, then unpause recording
                case RecordingStatus.PausedRecording:
                {
                    recordingStatus = RecordingStatus.IsRecording;
                    
                    WriteLineToMessageLog("Resumed recording");
                    record_Button.Content = "Pause Recording";


                    break;
                }
            }
        }


        //--- Video and Audio recording initialization ---//
        KinectAudioRecorder kinectAudioRecorder;

        void WavRecorder_initialize(string pathWoExtension)
        {
            filePath_wav = pathWoExtension + ".wav";
            kinectAudioRecorder = new KinectAudioRecorder(filePath_wav);
        }
        
        void AviRecorder_initialize(string pathWoExtension)
        {
            filePath_avi = pathWoExtension + ".avi";

            // https://sharpavi.codeplex.com/wikipage?title=Getting%20Started&referringTitle=Documentation
            aviWriter = new AviWriter(filePath_avi)
            {
                FramesPerSecond = FRAMES_PER_SECOND,
                // Emitting AVI v1 index in addition to OpenDML index (AVI v2)
                // improves compatibility with some software, including 
                // standard Windows programs like Media Player and File Explorer
                EmitIndex1 = true
            };

            // https://sharpavi.codeplex.com/wikipage?title=Using%20Video%20Encoders&referringTitle=Documentation
            var codecs = Mpeg4VideoEncoderVcm.GetAvailableCodecs();
            // Present available codecs to user or select programmatically
            // ...
            FourCC selectedCodec = KnownFourCCs.Codecs.Xvid;
            var encoder = new Mpeg4VideoEncoderVcm(FRAME_WIDTH, FRAME_HEIGHT,
                                                FRAMES_PER_SECOND, // frame rate
                                                0, // number of frames, if known beforehand, or zero
                                                70, // quality, though usually ignored :(
                                                selectedCodec // codecs preference
                                                );
            encodingStream = aviWriter.AddEncodingVideoStream(encoder, true, FRAME_WIDTH, FRAME_HEIGHT);                            
            //--- ---//
        }
        //--- ---//

        
        async void StopRecording_Click(object sender, EventArgs e)
        {
            if (recordingStatus != RecordingStatus.NotRecording)
            {
                recordingStatus = RecordingStatus.NotRecording;

                recording_Task = recording_Task.ContinueWith((t) =>
                {
                    bvhFile.closeBVHFile();
                    bvhFile = null;

                    skeletonRecorder.Close();
                    skeletonRecorder = null;

                    aviWriter.Close();
                    aviWriter = null;
                    encodingStream = null;

                    audioRecording_Task.Wait();  // wait for audio recording to finish recording audio
                    kinectAudioRecorder.Close();

                    try
                    {
                        //--- Merges AVI file and WAV file ---//
                        FFmpeg ffmpeg = new FFmpeg();

#if DEBUG
                        // debugging
                        double result1 = ffmpeg.GetFileDuration_ffprobe(filePath_avi);
                        double result2 = ffmpeg.GetFileDuration_ffprobe(filePath_wav);
#endif

                        ffmpeg.MergeWavWithAvi(filePath_avi, filePath_wav);
                    }
                    catch (Exception ex)
                    {
                        WriteLineToMessageLog(ex.Message);
                    }
                });
                await recording_Task;  // wait for completion of recording tasks before exiting

                WriteLineToMessageLog("Stopped recording");
                record_Button.Content = "Record";
            }

            // enable all windows (overridden close buttons) to be closed
            this.Closing -= WindowsCloseButton.DisableWindowClosing;
            
            CloseBackgroundWindows();
            MetadataWindow metaDataWindow = new MetadataWindow(filePath_base);
            this.Close();
            metaDataWindow.Show();
        }


        //--- Audio Recording ---//
        byte[] audioBuffer = new byte[1024];
        // initialize task
        Task audioRecording_Task = Task.Run(() => { });
        int numberBytesRead;

        void AudioStreamReader()
        {
            // polls for audio data until audioStream is closed
            while ((numberBytesRead = audioStream.Read(audioBuffer, 0, audioBuffer.Length)) > 0)
            {
                // records audio data if is recording and a skeleton is tracked
                if (recordingStatus == RecordingStatus.IsRecording && skeletonTracking.isSkeletonTracked)
                {
                    // pass state for task
                    byte[] audioBuffer_state = new byte[audioBuffer.Length];
                    audioBuffer.CopyTo(audioBuffer_state, 0);
                    int count_state = numberBytesRead;

                    // use separate Task for reading audio to avoid overflowing of reading task
                    audioRecording_Task = audioRecording_Task.ContinueWith((continuation) =>
                    {
                        kinectAudioRecorder.Write(audioBuffer_state, count_state);
                    });
                }
            }
        }
        //--- ---//
    }

    public static class ExtensionMethods
    {
        public static Skeleton Mirror(this Skeleton skeleton) {
            
            foreach (Joint joint in skeleton.Joints)
            {
                // http://www.neowin.net/forum/topic/1076807-c-kinect-skeleton-variable/
                var modifiedJoint = joint;
                var position = modifiedJoint.Position;
                position.X *= -1;
                modifiedJoint.Position = position;
                skeleton.Joints[joint.JointType] = modifiedJoint;
            }
            
            return skeleton;
        }
    }
}
